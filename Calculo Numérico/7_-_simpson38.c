#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#define n 7

double f(double x);
double erro(double exata, double aprox);
double simpsonsimp(double *x, double h);
double simpsoncomp(double *x, double h);

int main(void){
	
	double x[4] = {0,1.333333,2.666666,4};
	double y[7] = {0,0.666666,1.333332,1.999998,2.666664,3.333333,4};
	double h = 3;
	double aproxi = 0;
	double aproxii = 0;
	double integral = 3.500168;
	
    /* 2. Calcule a seguinte integral: */
    /* If = (1 - e^(-2x)) dx */
	/* f) por uma �nica aplica��o da regra de Simpson 3/8;   */
	printf("Resultado do metodo de Simpson3/8 simples \n");
	aproxi = simpsonsimp(x,1.333333);
	printf("Resultado = %f\n",aproxi);
	printf("erro = %f%%\n\n\n",erro(integral,aproxi));
	/* g) por aplica��o da regra de Simpson 3/8 composta, com N = 6;    */
    printf("Resultado do metodo de Simpson3/8 composta \n");
	aproxii = simpsoncomp(y,0.666666);
	printf("Resultado = %f\n",aproxii);
	printf("erro = %f%%",erro(integral,aproxii)); 
	return 0;
}

double f(double x){
	return (1 - (pow(M_E,-2*x)));
}

double erro(double exata, double aprox){
	return fabs(((exata-aprox)/exata)*100);
}

double simpsonsimp(double *x, double h){
	return 	(3*h/8)*(f(x[0]) + (3*f(x[1]))+ (3*f(x[2])) + f(x[3]));
}

double simpsoncomp(double *x, double h){
	double a = 0;
	double b = 0;
	int i = 0;
	int k = 0;
	
	for(i = 1; i < n ; i = i+3){
		a = a + (3*(f(x[i]) + f(x[i+1])));
	}
		for(k = 3;k < n-1;k = k+3){
		b = b + 2*f(x[k]);
	}
		 
	return (f(x[0]) + a + b + f(x[n-1]))*(3*h/8);
}



