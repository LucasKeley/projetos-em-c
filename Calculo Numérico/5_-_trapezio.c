#include<stdio.h>
#include<math.h>
#define n 5

double f(double x);

int main(void){
	
	/* 2. Calcule a seguinte integral: */
    /* If = (1 - e^(-2x)) dx */
	/* c) por aplica��o da regra do trap�zio composta, com  N = 4;  */
	int i = 0;
	double x[n] = {0,1,2,3,4};
	double soma = 0;
	double result = 0;
	double h = 0;
	double erro = 0;
	double integral = 3.500167731;
	
 for(i = 0; i < n-1 ;i++){
 	h = x[1] - x[0];
	result = ((f(x[i]) + f(x[i+1])) / 2)*h;
    soma = soma + result;
	erro = fabs((integral-soma)/integral)*100;  
	
 }
   printf("Resultado = [%f] erro = [%.2f%%]",soma,erro);
	return 0;
}


double f(double x){
	return (1-(pow(M_E,-2*x)));
}
