#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#define n 5

double f(double x);
double erro(double exata, double aprox);
double simpsonsimp(double *x, double h);
double simpsoncomp(double *x, double h);

int main(void){
	
	double x[3] = {0,2,4};
	double y[n] = {0,1,2,3,4};
	double aproxi = 0;
	double aproxii = 0;
	double integral = 3.500168;
	
    /* 2. Calcule a seguinte integral: */
    /* If = (1 - e^(-2x)) dx */
	/* d)  por uma �nica aplica��o da regra de Simpson 1/3;  */
	
	printf("Resultado do metodo de Simpson1/3 simples \n");
	aproxi = simpsonsimp(x,2);
	printf("Resultado = %f\n",aproxi);
	printf("erro = %f%%\n",erro(integral,aproxi));
	
	/* e) por aplica��o da regra de Simpson 1/3 composta, com N = 4; */
	printf("Resultado do metodo de Simpson1/3 composta \n");
	aproxii = simpsoncomp(y,1);
	printf("Resultado = %f\n",aproxii);
	printf("erro = %f%%",erro(integral,aproxii));
	
	return 0;
}

double f(double x){
	return (1 - (pow(M_E,-2*x)));
}

double erro(double exata, double aprox){
	return fabs(((exata-aprox)/exata)*100);
}

double simpsonsimp(double *x, double h){
	return (h/3)*(f(x[0])+(4*f(x[1]))+f(x[2]));
}

double simpsoncomp(double *x, double h){
	double a = 0;
	double b = 0;
	int i = 0;
	int k = 0;
	
	for(i = 1; i < n ; i = i+2){
		a = a + 4*f(x[i]);
	}
		for(k = 2;k < 4;k = k+2){
		b = b + 2*f(x[k]);
	}
		 
	return (f(x[0]) + a + b + f(x[n-1]))*(h/3);
}



