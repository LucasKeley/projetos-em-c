#include<stdio.h>
#include<math.h>

double f(double x);

int main(void){
	
	
	int n = 0;
	double an = 2;
	double bn = 3;
	double xn = 0;
	double fan = 0;
	double fxn = 0;
	double toler = 0.1;
	double e = 0;
	int i = 0;
	
	/*Utilizando o m�todo sa biss�ao, encontre uma raiz da fun��o (x^2 - 5) ; E[2:3]  e <= 0.1*/
	
	do{
	fan = f(an);
	xn = (an + bn)/2;
	e = fabs(bn - an)/2;
	fxn = f(xn);
	printf("[%d] -- An = [%f] -- Bn = [%f] -- Xn = [%f] -- f(An) = [%f] -- f(Xn) = [%f] -- Toler = [%f]\n",n , an, bn, xn, fan, fxn,e );
	if(fan*fxn < 0){
		bn = xn;
	}
	else{
		an = xn;
	}
	n++;
	}while(e > toler);
	
	return 0;
}

double f(double x){
	return (pow(x,2)-5);
}

