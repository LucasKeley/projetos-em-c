#include<stdio.h>
#include<math.h>

double f1(double x);
double f(double x);


int main(void){
	
	int i = 0;
	double xx = 0;
	double x1 = 1.5;
	double x2 = 1.4;
	double x3 = 0;
	double xant = 0;
	double exep = 0;
	double toler = 0.01;
	double e = 0;
	
	/*Utilizando o m�todo da secante, encontre uma raiz da fun��o ( raiz(x) - 5e^(-x)) ; E[1.5:1.4]  e <= 0.01*/
	do {
	
	xx = f(x1);
	x3 = (x2 - (f(x2)*(x1-x2))/(f(x1)-f(x2)));
	exep = xant;
	e = fabs(x1 - xant)/xant;
	
	printf("[%d] -- Xn =  [%f] -- f(Xn) = [%f] -- Toler [%f]\n", i, x1, xx, e);
	xant = x1;
	x1 = x2;
	x2 = x3;
	i++;		
			
	} while (e > toler);
	
	return 0;
}

double f1(double x){
	return pow(x,3) + (3*pow(x,2)) - 1;
}

double f(double x){
	return sqrt(x) - (5*(pow(M_E,-x)));
}
