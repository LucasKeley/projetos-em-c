#include<stdio.h>
#include<stdlib.h>
#include<math.h>

double funcao(double x);
double progressiva(double xcent, double xdep);
double regressiva(double xant, double xcent);
double centrista(double xant, double xcent, double xdep);
double erro(double deriv, double derivaprox);

int main(void){
	
	/* 1. Calcule as aproxima��es por diferen�as progressiva, regressiva e central 
	de primeira ordem para cada uma das seguintes fun��es nas posi��es especificadas 
	e para o tamanho de passo especificado:  */
	/* b) x^2*cos x  ;  em x = 0,4; h = 0,1 h*/
	
	printf("Derivada Progressiva\n");
	printf("%f\n",progressiva(0.5,0.4));
	printf("erro [%.2f%%]\n",erro(0.799155729,progressiva(0.5,0.4)));
	
	printf("Derivada Regressiva\n");
	printf("%f\n",regressiva(0.4,0.3));
	printf("erro [%.2f%%]\n",erro(0.799155729,regressiva(0.4,0.3)));
		
	printf("Derivada Centrista\n");
	printf("%f\n",centrista(0.3,0.4,0.5));
	printf("erro [%.2f%%]\n",erro(0.799155729,centrista(0.3,0.4,0.5)));
		
	return 0;
	
	}
	
	double funcao(double x){
		return pow(x,2)*cos(x); 
	}
	
double progressiva(double xcent, double xdep){
	double h = xdep - xcent;
	return (funcao(xdep) - funcao(xcent))/h;
}
double regressiva(double xant, double xcent){
	double h = xcent -  xant;
	return (funcao(xcent) - funcao(xant))/h;
}
double centrista(double xant, double xcent, double xdep){
	double h = xdep - xcent;
	return (funcao(xdep) - funcao(xant))/(2*h);
}

double erro(double deriv, double derivaprox){
	return fabs(((deriv - derivaprox) / deriv)*100);
}

