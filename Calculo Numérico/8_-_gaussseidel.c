#include<stdio.h>
#include<math.h>
#define n 6

int main(void){
	
	float a[5] = {9,-2,-3,2,54.5};
	float b[5] = {2,8,-2,3,-14};
	float c[5]= {-3,2,11,-4,12.5};
	float d[5] = {-2,3,2,10,-21};		
	float x[4] = {0,0,0,0};
	float xr[4] = {0,0,0,0};
	int i = 0;
	float e = 0;
	float toler = 0.0001;
    float antx;
    
    /* Exeemplo 1 do caderno referente ao metodo de GaussSeidel */
	do{
	antx = xr[0];	
	xr[0] = (((a[1]*x[1]*(-1)) + (a[2]*x[2]*(-1)) + (a[3]*x[3]*(-1)) + a[4])/a[0]);
	x[0] = xr[0];
	xr[1] = (((b[0]*x[0]*(-1)) + (b[2]*x[2]*(-1)) + (b[3]*x[3]*(-1)) + b[4])/b[1]);
	x[1] = xr[1];
	xr[2] = (((c[0]*x[0]*(-1)) + (c[1]*x[1]*(-1)) + (c[3]*x[3]*(-1)) + c[4])/c[2]);
	x[2] = xr[2];
	xr[3] = (((d[0]*x[0]*(-1)) + (d[1]*x[1]*(-1)) + (d[2]*x[2]*(-1)) + d[4])/d[3]);
	x[3] = xr[3];
	
	e = fabs(((antx - xr[0])/xr[0]));
	i++;
	}
	while(e > toler);
	for(i = 0; i < 4;i++){
		printf("x[%d] = [%f] \n",i+1,x[i]);
	}
	
	return 0;
}


